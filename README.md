# bantam-toc

## Purpose

The `bantam-toc` module provides a lightweight table of contents module for BantamJS. When links within the table of contents are clicked, the page will scroll to the appropriate section.

## Installation

The script can be downloaded [here](files/1.0/bantam-toc-1.0.min.js) and linked directly into your app (it requires [BantamJS](https://canderegg.gitlab.io/bantam-js/) as a prerequisite). Alternatively, it is a module available through the [Bantam Coop](https://canderegg.gitlab.io/bantam-coop/) module manager.

To install the module through Bantam Coop, use the following command:

```bash
$ python coop.py add bantam-toc +1.0
```

## Usage

The element containing the table of contents must be assigned the `bt-contents` class in CSS. This containing element should contain a collection of links, each with a hash ID reference to an element elsewhere in the page.

An example table of contents might look like this:

```html
<ul class="bt-contents">
  <li><a href="#section-1">Section 1</a></li>
  <li><a href="#section-2">Section 2</a></li>
  <li><a href="#section-3">Section 3</a></li>
</ul>

<h1 id="section-1">Section 1</h1>
<p>This is section 1!</p>

<h1 id="section-2">Section 2</h1>
<p>This is section 2!</p>

<h1 id="section-3">Section 3</h1>
<p>This is section 3!</p>
```

The module will automatically extract the links from the table of contents and add event handlers to scroll to the appropriate section when each is clicked.

In the event that padding is desired to prevent scrolling all the way to the top of the screen (i.e. if there is non-scrolling navbar or header information), that offset can be set as below:

```javascript
$bt.toc.offset = 80; // The offset from the top of the page, in pixels
```

## License

This module is distributed under the [MIT License](LICENSE).
