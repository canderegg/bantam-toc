(function(){
  $bt.toc = {};
  $bt.toc.offset = 0;

  let contents = $bt.get('.bt-contents');
  for (let i=0; i<contents.length; i++) {
    let links = contents[i].get('a');

    for (let li=0; li<links.length; li++) {
      links[li].onclick = function() {
        $bt.scroll($bt.get(this.hash).offsetTop - $bt.toc.offset, 300);
        return false;
      };
    }
  }

})();
